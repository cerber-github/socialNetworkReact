import React from "react";
import s from './Header.module.css';

const Header = () => {
    return <header className={s.Header}>
        <img src='https://upload.wikimedia.org/wikipedia/commons/7/7e/Falanster_logo_300x300.png'/>
    </header>
}

export default Header;